﻿//Program Details:
//PROJ 207 - Travel Experts C# Application
//This C# application will be used by Travel Experts employees to add, update, and delete data from their database. 
//Designed by: Sanaz Afsahi
//Coded by: Sanaz Afsahi
//If there more than one coder contributed, code authors will indicate their work within comments in the code. 

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Proj207
{
    public partial class frmPackages : Form
    {
        public frmPackages(frmContainer TravelMain)
        {
            InitializeComponent();
            MdiParent = TravelMain;
        }

        private void frmPackages_Load(object sender, EventArgs e)
        {
            {
                UpdateFormState(true, false, false, false, false);
                //packageID will be add to the table automaticly
                txtpackageId.ReadOnly = true;
                //call refresgdata method-when the pacge load you can see the list of all packages in the lstpackages
                refreshData();
                //call refreshCombo- you can see the list of all product supplier in the combo box
                RefreshCombo();
            }
        }
        //refreshCombo method- add the list of all product supplier in the list of combo box
        private void RefreshCombo()
        {
            DataAccess myDB = new DataAccess();
            //SelectProductSupplier mthod- the out put of this method is the list of all product supplier
            List<Product_Supplier_Complete> list1 = myDB.SelectProductSupplier();
            //add this output list to the combo box
            cboProductSuppliers.DataSource = list1;
            //by default we don't want  selected product supplier in combo box
            cboProductSuppliers.SelectedIndex = -1;

        }

        //Updates the state of the buttons and may clear the selected item in the products list
        private void UpdateFormState(bool newEnabled, bool saveEnabled, bool updateEnabled, bool deleteEnabled, bool clearSelectedItem)
        {
            btnNew.Enabled = newEnabled;
            btnAdd.Enabled = saveEnabled;
            btnUpdate.Enabled = updateEnabled;
            btnDelete.Enabled = deleteEnabled;

        }
        //refreshMethod-for refreshing data in the packages list
        private void refreshData()
        {
            try
            {
                DataAccess myDB = new DataAccess();
                //add the output of select method(list of all packages) in to the lstpackages
                lstPackages.DataSource = myDB.Select();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void lstPackages_SelectedIndexChanged(object sender, EventArgs e)
        {
            // is something selected?  If not, exit
            if (lstPackages.SelectedIndex == -1)
                return;

            // instantiate an oilwell
            Packages Pack = new Packages();
            //selectItem return an Object, so we need to cast
            Pack = (Packages)lstPackages.SelectedItem;
            txtpackageId.Text = Pack.packageId.ToString();
            txtPackName.Text = Pack.packageName;
            dtpStartDate.Value = Pack.pkgStartDate;
            dtpEndDate.Value = Pack.pkgEndDate;
            txtDesc.Text = Pack.pkgDescrip;
            txtPrice.Text = Pack.pkgBasePrice.ToString();
            txtCommission.Text = Pack.pkgAgencyCommission.ToString();
            UpdateFormState(true, false, true, true, false);
            //----product-supplier
            DataAccess myDB = new DataAccess();
            List<Product_Supplier_Complete> myPSC = new List<Product_Supplier_Complete>();
            myPSC = myDB.showProductsSuppliers(Pack);
            lstProductSupplier.DataSource = myPSC;

        }
        //new button click-make the text boxes emty with clearform method
        private void btnNewPackage_Click(object sender, EventArgs e)
        {
            clearForm();
            //after clicking new button, add button will be active
            UpdateFormState(true, true, false, false, true);
        }
        //making the text boxes empty 
        private void clearForm()
        {

            foreach (Control ctrl in this.grpboxPackage.Controls)
            {

                if (ctrl.GetType() == typeof(TextBox))
                    ctrl.Text = "";
                //chnage the datse to current date
                dtpStartDate.Value = DateTime.Now;
                dtpEndDate.Value = DateTime.Now;
                txtpackageId.Text = "";
                txtpackageId.ReadOnly = true;

            }
        }

        //add button click-you can use add button after clicking the new button
        private void btnAddPackage_Click(object sender, EventArgs e)
        {
            //checking the data validation with calling the validation method 
            if (validation())
            {
                //make an object form packages class
                Packages myPackages = new Packages();
                //entering data to the object properties
                myPackages.packageName = txtPackName.Text;
                myPackages.pkgStartDate = dtpStartDate.Value;
                myPackages.pkgEndDate = dtpEndDate.Value;
                myPackages.pkgDescrip = txtDesc.Text;
                myPackages.pkgBasePrice = decimal.Parse(txtPrice.Text);
                myPackages.pkgAgencyCommission = decimal.Parse(txtCommission.Text);
                try
                {
                    DataAccess DB = new DataAccess();
                    //call insert method for inserting the object to the packages table
                    DB.Insert(myPackages);
                    MessageBox.Show("Data inserted.");
                    UpdateFormState(true, false, false, false, true);
                    refreshData();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }
        // validation method(befor adding or updationg data this method will be called)
        private bool validation()
        {
            try
            {
                //package name must have a value
                if (txtPackName.Text == "")
                {
                    MessageBox.Show("please enter value for packeage name");
                    return false;
                }
                // packeage description must have a value
                if (txtDesc.Text == "")
                {
                    MessageBox.Show("please enter value for packeage description");
                    return false;
                }

                decimal myDec;
                if (!decimal.TryParse(txtPrice.Text, out myDec))
                {
                    MessageBox.Show("please inter decimal value for Baseprice.(Agency Commision can not greater than the base Price)");
                    return false;
                }
                decimal myDeci;
                if (!decimal.TryParse(txtCommission.Text, out myDeci))
                {
                    MessageBox.Show("please inter decimal value for commission.(Agency Commision can not greater than the base Price)");
                    return false;
                }
                if (decimal.Parse(txtCommission.Text) > decimal.Parse(txtPrice.Text))
                {
                    MessageBox.Show("Agency Commision can not greater than the base Price");
                    return false;
                }

                if (dtpStartDate.Value >= dtpEndDate.Value)
                {
                    MessageBox.Show("End Date must be later than start date");
                    return false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return true;
        }
        //Update button-for updating the data of a package
        private void btnUpdate_Click(object sender, EventArgs e)
        {
            //at first you should select a package from the list of packages
            if (lstPackages.SelectedItems.Count == 0)
            {
                MessageBox.Show("please select an item for updating");
                return;
            }
            //ckecking the valodation of datas 
            if (validation())
            {
                //set a object of package class
                Packages myPackages = new Packages();
                //set the value of input text boxes to the properties of the packages object
                myPackages.packageId = Convert.ToInt32(txtpackageId.Text);
                myPackages.packageName = txtPackName.Text;
                myPackages.pkgStartDate = dtpStartDate.Value;
                myPackages.pkgEndDate = dtpEndDate.Value;
                myPackages.pkgDescrip = txtDesc.Text;
                myPackages.pkgBasePrice = decimal.Parse(txtPrice.Text);
                myPackages.pkgAgencyCommission = decimal.Parse(txtCommission.Text);
                try
                {
                    DataAccess DB = new DataAccess();
                    //call the updating method
                    DB.Update(myPackages);
                    MessageBox.Show("Update done!");
                    UpdateFormState(true, false, false, false, true);
                    refreshData();

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }
        //delete button for deleting selected package from Data Base 
        private void btnDelete_Click(object sender, EventArgs e)
        {
            //at first you have to select a package from the list
            if (lstPackages.SelectedIndex == -1)
            {
                MessageBox.Show("please select an item");
                return;
            }
            //be sure about deleting 
            if (MessageBox.Show("Do you really want to delete?", "Delete?", MessageBoxButtons.YesNo,
                MessageBoxIcon.Question) == DialogResult.Yes)
            {
                Packages myPack = new Packages();
                myPack.packageId = int.Parse(txtpackageId.Text);
                try
                {
                    DataAccess Db = new DataAccess();
                    //calling the delete method 
                    Db.Delete(myPack);
                    UpdateFormState(true, false, false, false, true);
                    refreshData();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        //AddProductSupplier button- for adding a product-supplier to a package
        private void btnAddProductSupplier_Click(object sender, EventArgs e)
        {
            if (validationComboList())
            {
                //make a package object from packages class
                Packages myPackages = new Packages();
                //set the selected package from the packages list to the object
                myPackages = (Packages)lstPackages.SelectedItem;
                //make  myPSC object form Product_Supplier_Complete oclass
                Product_Supplier_Complete myPSC = new Product_Supplier_Complete();
                //add selected product-supplier to myPSC Object
                myPSC = (Product_Supplier_Complete)cboProductSuppliers.SelectedItem;

                DataAccess DB = new DataAccess();
                DB.InsertPS(myPackages, myPSC);
                MessageBox.Show("selected product-supplier added to your packeage.");
                refreshData();

            }
        }

        private bool validationComboList()
        {
            //you have to slect a product supplier from the combo box list
            try
            {
                if (cboProductSuppliers.SelectedIndex == -1)
                {
                    MessageBox.Show("please select a product supplier from  product supplier list");
                    return false;
                }
                //you have to slect a package from the packages list
                if (lstPackages.SelectedIndex == -1)
                {
                    MessageBox.Show("please select a package from the package lsit");
                    return false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return true;
        }
        //remove seleted product supplier from selected package

        private void btnRemoveProduct_Click(object sender, EventArgs e)
        {
            //you have to select a product supplier
            if (lstProductSupplier.SelectedIndex == -1)
            {
                MessageBox.Show("please select a Product-supplier that you want to delete it from a package");
                return;
            }
            // you have to select a package from the list
            if (lstPackages.SelectedIndex == -1)
            {
                MessageBox.Show("please select a Package that you want to delete thid selected product-supplier from it");
                return;
            }
            //be sure about deleting 
            if (MessageBox.Show("Do you really want to delete?", "Delete?", MessageBoxButtons.YesNo,
                MessageBoxIcon.Question) == DialogResult.Yes)
            {
                //make myPSC object from Product_Supplier_Complete class
                Product_Supplier_Complete myPSC = new Product_Supplier_Complete();
                //add the selected product supplier to this object(myPSC)
                myPSC = (Product_Supplier_Complete)lstProductSupplier.SelectedItem;
                //make myPS object from productSupplier class
                productSupplier myPS = new productSupplier();
                //adding the value to myPS object-myPSC.ps is a type of productSupplie class
                myPS = myPSC.ps;
                //make a package object()pack
                Packages pack = new Packages();
                //add selected packag to this object(pack)
                pack = (Packages)lstPackages.SelectedItem;

                DataAccess Db = new DataAccess();
                //call the RemovepsFromPackage(pack, myPS) method that has to input data:(an object 
                //of packages class, an object of productsuplier class )
                Db.RemovepsFromPackage(pack, myPS);
                List<Product_Supplier_Complete> myPSCList = new List<Product_Supplier_Complete>();
                //call the showProductsSuppliers method-the input of this method is a package and the out put is the list 
                //of included produst-supplier for this package-adding this out put to myPSCList lsit
                myPSCList = Db.showProductsSuppliers(pack);
                //add this list in to the lstProductSupplier
                lstProductSupplier.DataSource = myPSCList;

            }
        }
        private void Refreshlist()
        {
            DataAccess myDB = new DataAccess();
            List<Product_Supplier_Complete> list1 = myDB.SelectProductSupplier();
            lstProductSupplier.DataSource = list1;
            lstProductSupplier.SelectedIndex = -1;
        }

    }
}
