﻿namespace Proj207
{
    partial class frmSuppliers
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lstSuppliers = new System.Windows.Forms.ListBox();
            this.grpboxSupInfo = new System.Windows.Forms.GroupBox();
            this.lblSupId = new System.Windows.Forms.Label();
            this.lblSupName = new System.Windows.Forms.Label();
            this.txtSupId = new System.Windows.Forms.TextBox();
            this.txtSupName = new System.Windows.Forms.TextBox();
            this.grpboxSupTools = new System.Windows.Forms.GroupBox();
            this.btnSupNew = new System.Windows.Forms.Button();
            this.btnSupAdd = new System.Windows.Forms.Button();
            this.btnSupUpdate = new System.Windows.Forms.Button();
            this.btnSupDelete = new System.Windows.Forms.Button();
            this.grpboxSupInfo.SuspendLayout();
            this.grpboxSupTools.SuspendLayout();
            this.SuspendLayout();
            // 
            // lstSuppliers
            // 
            this.lstSuppliers.FormattingEnabled = true;
            this.lstSuppliers.Location = new System.Drawing.Point(260, 18);
            this.lstSuppliers.Name = "lstSuppliers";
            this.lstSuppliers.Size = new System.Drawing.Size(266, 199);
            this.lstSuppliers.TabIndex = 0;
            this.lstSuppliers.SelectedIndexChanged += new System.EventHandler(this.lstSuppliers_SelectedIndexChanged);
            // 
            // grpboxSupInfo
            // 
            this.grpboxSupInfo.Controls.Add(this.lblSupId);
            this.grpboxSupInfo.Controls.Add(this.lblSupName);
            this.grpboxSupInfo.Controls.Add(this.txtSupId);
            this.grpboxSupInfo.Controls.Add(this.txtSupName);
            this.grpboxSupInfo.Location = new System.Drawing.Point(12, 12);
            this.grpboxSupInfo.Name = "grpboxSupInfo";
            this.grpboxSupInfo.Size = new System.Drawing.Size(242, 100);
            this.grpboxSupInfo.TabIndex = 1;
            this.grpboxSupInfo.TabStop = false;
            this.grpboxSupInfo.Text = "Supplier Information";
            // 
            // lblSupId
            // 
            this.lblSupId.AutoSize = true;
            this.lblSupId.Location = new System.Drawing.Point(39, 25);
            this.lblSupId.Name = "lblSupId";
            this.lblSupId.Size = new System.Drawing.Size(59, 13);
            this.lblSupId.TabIndex = 0;
            this.lblSupId.Text = "Supplier ID";
            // 
            // lblSupName
            // 
            this.lblSupName.AutoSize = true;
            this.lblSupName.Location = new System.Drawing.Point(22, 52);
            this.lblSupName.Name = "lblSupName";
            this.lblSupName.Size = new System.Drawing.Size(76, 13);
            this.lblSupName.TabIndex = 1;
            this.lblSupName.Text = "Supplier Name";
            // 
            // txtSupId
            // 
            this.txtSupId.Location = new System.Drawing.Point(104, 22);
            this.txtSupId.Name = "txtSupId";
            this.txtSupId.Size = new System.Drawing.Size(132, 20);
            this.txtSupId.TabIndex = 2;
            // 
            // txtSupName
            // 
            this.txtSupName.Location = new System.Drawing.Point(104, 49);
            this.txtSupName.Name = "txtSupName";
            this.txtSupName.Size = new System.Drawing.Size(132, 20);
            this.txtSupName.TabIndex = 3;
            // 
            // grpboxSupTools
            // 
            this.grpboxSupTools.Controls.Add(this.btnSupNew);
            this.grpboxSupTools.Controls.Add(this.btnSupAdd);
            this.grpboxSupTools.Controls.Add(this.btnSupUpdate);
            this.grpboxSupTools.Controls.Add(this.btnSupDelete);
            this.grpboxSupTools.Location = new System.Drawing.Point(12, 118);
            this.grpboxSupTools.Name = "grpboxSupTools";
            this.grpboxSupTools.Size = new System.Drawing.Size(242, 100);
            this.grpboxSupTools.TabIndex = 0;
            this.grpboxSupTools.TabStop = false;
            this.grpboxSupTools.Text = "Supplier Tools";
            // 
            // btnSupNew
            // 
            this.btnSupNew.Location = new System.Drawing.Point(25, 31);
            this.btnSupNew.Name = "btnSupNew";
            this.btnSupNew.Size = new System.Drawing.Size(75, 23);
            this.btnSupNew.TabIndex = 2;
            this.btnSupNew.Text = "New";
            this.btnSupNew.UseVisualStyleBackColor = true;
            this.btnSupNew.Click += new System.EventHandler(this.btnSupNew_Click);
            // 
            // btnSupAdd
            // 
            this.btnSupAdd.Location = new System.Drawing.Point(25, 60);
            this.btnSupAdd.Name = "btnSupAdd";
            this.btnSupAdd.Size = new System.Drawing.Size(75, 23);
            this.btnSupAdd.TabIndex = 3;
            this.btnSupAdd.Text = "Add";
            this.btnSupAdd.UseVisualStyleBackColor = true;
            this.btnSupAdd.Click += new System.EventHandler(this.btnSupAdd_Click);
            // 
            // btnSupUpdate
            // 
            this.btnSupUpdate.Location = new System.Drawing.Point(142, 31);
            this.btnSupUpdate.Name = "btnSupUpdate";
            this.btnSupUpdate.Size = new System.Drawing.Size(75, 23);
            this.btnSupUpdate.TabIndex = 4;
            this.btnSupUpdate.Text = "Update";
            this.btnSupUpdate.UseVisualStyleBackColor = true;
            this.btnSupUpdate.Click += new System.EventHandler(this.btnSupUpdate_Click);
            // 
            // btnSupDelete
            // 
            this.btnSupDelete.Location = new System.Drawing.Point(142, 60);
            this.btnSupDelete.Name = "btnSupDelete";
            this.btnSupDelete.Size = new System.Drawing.Size(75, 23);
            this.btnSupDelete.TabIndex = 5;
            this.btnSupDelete.Text = "Delete";
            this.btnSupDelete.UseVisualStyleBackColor = true;
            this.btnSupDelete.Click += new System.EventHandler(this.btnSupDelete_Click);
            // 
            // frmSuppliers
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(538, 238);
            this.Controls.Add(this.grpboxSupTools);
            this.Controls.Add(this.grpboxSupInfo);
            this.Controls.Add(this.lstSuppliers);
            this.Name = "frmSuppliers";
            this.Text = "Supplier Management";
            this.Load += new System.EventHandler(this.frmSuppliers_Load);
            this.grpboxSupInfo.ResumeLayout(false);
            this.grpboxSupInfo.PerformLayout();
            this.grpboxSupTools.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox lstSuppliers;
        private System.Windows.Forms.GroupBox grpboxSupInfo;
        private System.Windows.Forms.GroupBox grpboxSupTools;
        private System.Windows.Forms.Label lblSupId;
        private System.Windows.Forms.Label lblSupName;
        private System.Windows.Forms.TextBox txtSupId;
        private System.Windows.Forms.TextBox txtSupName;
        private System.Windows.Forms.Button btnSupNew;
        private System.Windows.Forms.Button btnSupAdd;
        private System.Windows.Forms.Button btnSupUpdate;
        private System.Windows.Forms.Button btnSupDelete;
    }
}