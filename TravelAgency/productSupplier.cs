﻿//Program Details:
//PROJ 207 - Travel Experts C# Application
//Coded by: Sanaz Afsahi
//If there more than one coder contributed, code authors will indicate their work within comments in the code. 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Proj207
{
   public class productSupplier
    {
       //encapsulate properties

        private int _productSupplierID;
        private int _supplierID;
        private int _productId;
       
       //get-set
        public int supplierID
        {
            get { return _supplierID; }
            set { _supplierID = value; }
        }

        public int  productSupplierID
        {
            get { return _productSupplierID; }
            set { _productSupplierID = value; }
        }

        public int productId
        {
            get { return _productId; }
            set { _productId = value; }
        }
        
        public override string ToString()
        {
            return string.Format("-{0}-{1}-{2}-",productSupplierID, supplierID, productId);
        }


    }
}
