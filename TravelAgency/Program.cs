﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms; 

namespace Proj207
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the Travel Angency Application
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new frmContainer());
        }
    }
}
