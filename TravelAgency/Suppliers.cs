﻿//Program Details:
//PROJ 207 - Travel Experts C# Application
//Coded by: Sanaz Afsahi
//If there more than one coder contributed, code authors will indicate their work within comments in the code. 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Proj207
{
    public class Suppliers
    {
        //encapsulate properties
        private int _supplierID;
        private string _suplierName;

        //get-set
        public int supplierID
        {
            get { return _supplierID; }
            set { _supplierID = value; }
        }

        public string suplierName
        {
            get { return _suplierName; }
            set { _suplierName = value; }
        }

        public override string ToString()
        {
            return string.Format("-{0}-{1}-", supplierID, suplierName);
        }



    }
}
