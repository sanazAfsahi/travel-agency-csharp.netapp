﻿//Program Details:
//PROJ 207 - Travel Experts C# Application
//Coded by: Sanaz Afsahi
//If there more than one coder contributed, code authors will indicate their work within comments in the code. 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;

namespace Proj207
{
    public class DataAccess
    {
        //encapsulated variables
        private SqlConnection _myConn;
        private SqlCommand _myCommand;

        //Constructor
        public DataAccess()
        {
            //instantiate & initalize the SqlConnection & SqlCommand Class
            _myConn = new SqlConnection("server=(local);database=TravelExperts;integrated security=SSPI");
            _myCommand = new SqlCommand();
            _myCommand.Connection = _myConn;
        }

        //select Method-select all the packages in the packages table
        public List<Packages> Select()
        {
            List<Packages> myPackages = new List<Packages>();
            // set the command text (sql) on the command obj
            _myCommand.CommandText = "SELECT * FROM Packages";

            try
            {
                _myConn.Open();
                SqlDataReader reader = _myCommand.ExecuteReader();

                //read the reader
                while (reader.Read())
                {
                    Packages myPack = new Packages();
                    myPack.packageId = reader.GetInt32(0);
                    myPack.packageName = reader.GetString(1);
                    myPack.pkgStartDate = reader.GetDateTime(2);
                    myPack.pkgEndDate = reader.GetDateTime(3);
                    myPack.pkgDescrip = reader.GetString(4);
                    myPack.pkgBasePrice = reader.GetDecimal(5);
                    myPack.pkgAgencyCommission = reader.GetDecimal(6);

                    //add my Pack to the List collection
                    myPackages.Add(myPack);
                }

            }
            catch (SqlException ex)
            {

                throw ex;
            }
            finally
            {
                _myConn.Close();
            }

            return myPackages;
        }


        //Inserting Method- for inserting the properties value of a package object to the package table
        public void Insert(Packages myPack)
        {
            //command of inserting data to the packages table
            string strSql = "Insert Into Packages (PkgName, PkgStartDate, PkgEndDate, PkgDesc, PkgBasePrice, PkgAgencyCommission) " +
                    " Values (@packageName, @pkgStartDate, @pkgEndDate, @pkgDescrip, @pkgBasePrice, @pkgAgencyCommission) ";
            SqlCommand _myCommand = new SqlCommand(strSql, _myConn);
            _myCommand.Parameters.AddWithValue("@packageName", myPack.packageName);
            _myCommand.Parameters.AddWithValue("@pkgStartDate", myPack.pkgStartDate);
            _myCommand.Parameters.AddWithValue("@pkgEndDate", myPack.pkgEndDate);
            _myCommand.Parameters.AddWithValue("@pkgDescrip", myPack.pkgDescrip);
            _myCommand.Parameters.AddWithValue("@pkgBasePrice", myPack.pkgBasePrice);
            _myCommand.Parameters.AddWithValue("@pkgAgencyCommission", myPack.pkgAgencyCommission);
            try
            {
                _myConn.Open();
                _myCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                _myConn.Close();
            }
        }

        public void Update(Packages myPack)
        {
            //command for updating data in packages table
            string strSql = "Update Packages Set PkgName=@packageName,PkgStartDate=@pkgStartDate,PkgEndDate=@pkgEndDate," +
                " PkgDesc=@pkgDescrip,PkgBasePrice=@pkgBasePrice,PkgAgencyCommission=@pkgAgencyCommission Where PackageId=@packageId";
            SqlCommand _myCommand = new SqlCommand(strSql, _myConn);

            _myCommand.Parameters.AddWithValue("@packageId", myPack.packageId);
            _myCommand.Parameters.AddWithValue("@packageName", myPack.packageName);
            _myCommand.Parameters.AddWithValue("@pkgStartDate", myPack.pkgStartDate);
            _myCommand.Parameters.AddWithValue("@pkgEndDate", myPack.pkgEndDate);
            _myCommand.Parameters.AddWithValue("@pkgDescrip", myPack.pkgDescrip);
            _myCommand.Parameters.AddWithValue("@pkgBasePrice", myPack.pkgBasePrice);
            _myCommand.Parameters.AddWithValue("@pkgAgencyCommission", myPack.pkgAgencyCommission);
            try
            {
                _myConn.Open();
                _myCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                _myConn.Close();
            }
        }

        //delete method for deleting 
        public void Delete(Packages Pack)
        {
            //set the delete command
            string strSql = "Delete from Packages Where  PackageId=@packageId";
            SqlCommand cm = new SqlCommand(strSql, _myConn);
            cm.Parameters.AddWithValue("@packageId", Pack.packageId);
            try
            {
                _myConn.Open();
                cm.ExecuteNonQuery();
                MessageBox.Show("deleted!");
            }
            catch (SqlException ex)
            {

                if (ex.ErrorCode == -2146232060)
                {
                    //this error means that this package is booked for a customer so we can not delete it
                    //but if we have a package that isn't in any booking , we can delete it
                    MessageBox.Show("You can not delete this Packages, because of existing transaction");
                }
                else

                    MessageBox.Show(ex.ErrorCode.ToString());
            }
            finally
            {
                _myConn.Close();
            }
        }

        public List<Pack_Prod_Supp> Search(string strPackage)
        {
            List<Pack_Prod_Supp> lst = new List<Pack_Prod_Supp>();
            SqlCommand cmdPro = new SqlCommand();
            string strSql = "Select * From Packages_Products_Suppliers Where 1=1 ";
            if (strPackage != "")
            {
                strSql += " And PackageId=@PackageID";
                cmdPro.Parameters.AddWithValue("@PackageID", strPackage);
            }

            try
            {
                cmdPro.Connection = _myConn;
                cmdPro.CommandText = strSql;
                _myConn.Open();
                SqlDataReader rd = cmdPro.ExecuteReader();
                while (rd.Read())
                {
                    Pack_Prod_Supp pps = new Pack_Prod_Supp();
                    pps.packageId = (int)rd["PackageId"];
                    pps.productSupplierID = (int)rd["ProductSupplierId"];
                    lst.Add(pps);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                _myConn.Close();
            }
            return lst;
        }


        //the out put of this method is the list of all included product supplier for a package

        public List<Product_Supplier_Complete> showProductsSuppliers(Packages pkg)
        {
            //PSC_LIST is a list of Product_Supplier_Complete objects
            List<Product_Supplier_Complete> PSC_LIST = new List<Product_Supplier_Complete>();
            //set the command fo rthis method
            string cmd = "SELECT P.ProductId,P.ProdName,S.SupplierId, S.SupName, PS.ProductSupplierId FROM Packages_Products_Suppliers PPS,Products_Suppliers PS,Suppliers S,Products P,Packages PKG Where PS.ProductId = P.ProductId and PS.SupplierId = s.SupplierId and PS.ProductSupplierId = PPS.ProductSupplierId and PKG.packageId = PPS.packageID and Pkg.packageId = @PackageID";
            //pass the parameter to sql
            _myCommand.CommandText = cmd;
            _myCommand.Parameters.AddWithValue("@packageid", pkg.packageId);
            try
            {
                _myConn.Open();
                SqlDataReader reader = _myCommand.ExecuteReader();

                //read the reader
                while (reader.Read())
                {
                    Products myProduct = new Products();
                    Suppliers mySupplier = new Suppliers();
                    productSupplier MyproductSupplier = new productSupplier();
                    Product_Supplier_Complete PSC = new Product_Supplier_Complete();
                    mySupplier.supplierID = reader.GetInt32(2);
                    mySupplier.suplierName = reader.GetString(3);
                    myProduct.productId = reader.GetInt32(0);
                    myProduct.productName = reader.GetString(1);
                    MyproductSupplier.productSupplierID = reader.GetInt32(4);
                    MyproductSupplier.productId = reader.GetInt32(0);
                    MyproductSupplier.supplierID = reader.GetInt32(2);

                    PSC.pr = myProduct;
                    PSC.sp = mySupplier;
                    PSC.ps = MyproductSupplier;
                    PSC_LIST.Add(PSC);

                }
            }
            catch (SqlException ex)
            {

                throw ex;
            }
            finally
            {
                _myConn.Close();
            }

            return PSC_LIST;
        }
        //SelectProductSupplier method- the output of this method is the list of the all product supplier

        public List<Product_Supplier_Complete> SelectProductSupplier()
        {
            List<Product_Supplier_Complete> myPSList = new List<Product_Supplier_Complete>();
            string cmd = "SELECT P.ProductId,P.ProdName,S.SupplierId, S.SupName, PS.ProductSupplierId FROM Products_Suppliers PS,Suppliers S,Products P Where PS.ProductId = P.ProductId and PS.SupplierId = s.SupplierId";
            _myCommand.CommandText = cmd;
            try
            {
                _myConn.Open();
                SqlDataReader reader = _myCommand.ExecuteReader();

                //read the reader
                while (reader.Read())
                {
                    Products myProduct = new Products();
                    Suppliers mySupplier = new Suppliers();
                    productSupplier myPS = new productSupplier();
                    Product_Supplier_Complete PSC2 = new Product_Supplier_Complete();
                    mySupplier.supplierID = reader.GetInt32(2);
                    mySupplier.suplierName = reader.GetString(3);
                    myProduct.productId = reader.GetInt32(0);
                    myProduct.productName = reader.GetString(1);
                    myPS.productSupplierID = reader.GetInt32(4);
                    myPS.productId = reader.GetInt32(0);
                    myPS.supplierID = reader.GetInt32(2);
                    PSC2.pr = myProduct;
                    PSC2.sp = mySupplier;
                    PSC2.ps = myPS;
                    myPSList.Add(PSC2);
                }
            }

            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                _myConn.Close();
            }
            return myPSList;
        }

        //inserting product supplier to the data base

        public void InsertPS(Packages myPackages, Product_Supplier_Complete myPSC)
        {
            productSupplier myPS = new productSupplier();
            myPS = myPSC.ps;

            string strSql = "Insert Into Packages_Products_Suppliers (PackageId, ProductSupplierId) Values (@PackageId, @ProductSupplierId)";
            SqlCommand _myCommand = new SqlCommand(strSql, _myConn);
            _myCommand.Parameters.AddWithValue("@PackageId", myPackages.packageId);
            _myCommand.Parameters.AddWithValue("@ProductSupplierId", myPS.productSupplierID);
            try
            {
                _myConn.Open();
                _myCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                _myConn.Close();
            }
        }
        // product-supplier form
        public List<Products> SelectProducts()
        {
            List<Products> myCategories = new List<Products>();
            // set the command text (sql) on the command obj
            _myCommand.CommandText = "Select ProductId, " +
                "ProdName from Products";
            try
            {
                _myConn.Open();
                SqlDataReader reader = _myCommand.ExecuteReader();

                //read the reader
                while (reader.Read())
                {
                    Products myCategory = new Products();
                    myCategory.productId = reader.GetInt32(0);
                    myCategory.productName = reader.GetString(1);

                    //add myCategory to the List collection
                    myCategories.Add(myCategory);
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                _myConn.Close();
            }
            return myCategories;
        }

        //Retireve a list of  Supplier objects from the database in a List collection
        public List<Suppliers> SelectSupplier()
        {
            List<Suppliers> mySuppliers = new List<Suppliers>();
            // set the command text (sql) on the command obj
            _myCommand.CommandText = "Select SupplierId, " +
                "SupName from Suppliers";
            try
            {
                _myConn.Open();
                SqlDataReader reader = _myCommand.ExecuteReader();

                //read the reader
                while (reader.Read())
                {
                    Suppliers mySupplier = new Suppliers();
                    mySupplier.supplierID = reader.GetInt32(0);
                    mySupplier.suplierName = reader.GetString(1);

                    //add myCategory to the List collection
                    mySuppliers.Add(mySupplier);
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                _myConn.Close();
            }

            return mySuppliers;
        }

        //method for conection suplier and product
        public void InsertPtoS(Products myProduct, Suppliers mySupplier)
        {
            //set the sql command
            string strSql = "Insert Into Products_Suppliers (ProductId, SupplierId) Values (@ProductId, @SupplierId)";
            SqlCommand _myCommand = new SqlCommand(strSql, _myConn);
            _myCommand.Parameters.AddWithValue("@ProductId", myProduct.productId);
            _myCommand.Parameters.AddWithValue("@SupplierId", mySupplier.supplierID);
            try
            {
                _myConn.Open();
                _myCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                _myConn.Close();
            }
        }
        //----removing product-supplier from a package(from Packages_Products_Suppliers table )

        public void RemovepsFromPackage(Packages pack, productSupplier ps)
        {
            string strSql = "Delete from Packages_Products_Suppliers Where PackageId=@PackageId and ProductSupplierId=@ProductSupplierId";
            SqlCommand cm = new SqlCommand(strSql, _myConn);
            cm.Parameters.AddWithValue("@PackageId", pack.packageId);
            cm.Parameters.AddWithValue("@ProductSupplierId", ps.productSupplierID);

            try
            {
                _myConn.Open();
                cm.ExecuteNonQuery();
                MessageBox.Show("deleted!");
            }
            catch (SqlException ex)
            {
                if (ex.ErrorCode == -2146232060)
                {

                    MessageBox.Show("You can not delete this product-supplier, because of existing transaction");
                }
                else

                    MessageBox.Show(ex.ErrorCode.ToString());
            }
            finally
            {
                _myConn.Close();
            }
        }


        //----removing product-supplier from Product-supplier table
        public void DeletePS(productSupplier ps)
        {
            string strSql = "Delete from Products_Suppliers Where ProductSupplierId=@ProductSupplierId";
            SqlCommand cm = new SqlCommand(strSql, _myConn);
            cm.Parameters.AddWithValue("@ProductSupplierId", ps.productSupplierID);
            try
            {
                _myConn.Open();
                cm.ExecuteNonQuery();
                MessageBox.Show("deleted!");
            }
            catch (SqlException ex)
            {
                if (ex.ErrorCode == -2146232060)
                {
                    //this error means that this product-supplier is selected for a package so we can not delete it
                    //but if we have a product-supplier that isn't in any packages , we can delete it
                    MessageBox.Show("You can not delete this product-supplier, because of existing transaction");
                }
                else

                    MessageBox.Show(ex.ErrorCode.ToString());
            }
            finally
            {
                _myConn.Close();
            }
        }


    }
}
