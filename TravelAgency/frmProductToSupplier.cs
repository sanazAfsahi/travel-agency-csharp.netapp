﻿//Program Details:
//PROJ 207 - Travel Experts C# Application
//This C# application will be used by Travel Experts employees to add, update, and delete data from their database. 
//Designed by: Sanaz Afsahi
//Coded by: Sanaz Afsahi
//If there more than one coder contributed, code authors will indicate their work within comments in the code. 

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Proj207
{
    public partial class frmProductToSupplier : Form
    {
        public frmProductToSupplier(frmContainer TravelMain)
        {
            InitializeComponent();
            MdiParent = TravelMain;
        }

        private void frmProductToSupplier_Load(object sender, EventArgs e)
        {
            //call the refreshcombo method:
            RefreshCombos();
            //call refreshlist method:
            Refreshlist();
        }
        //this method will be bring the list of all products and the list of all supplier in to their combo boxesS
        private void RefreshCombos()
        {
            DataAccess myDB = new DataAccess();
            List<Products> productlist = myDB.SelectProducts();
            cmbProduct.DataSource = productlist;
            cmbProduct.SelectedIndex = -1;

            List<Suppliers> supplierlist = myDB.SelectSupplier();
            cmbSupplier.DataSource = supplierlist;
            cmbSupplier.SelectedIndex = -1;

        }

        private void Refreshlist()
        {
            DataAccess myDB = new DataAccess();
            //call SelectProductSupplier method to bring all the product supplier in to the list(lstproductSupplier)
            List<Product_Supplier_Complete> list1 = myDB.SelectProductSupplier();
            lstProductSupplier.DataSource = list1;
            lstProductSupplier.SelectedIndex = -1;
        }

        //-------------------AddProductSupplier---------
        private void btnPtoS_Click(object sender, EventArgs e)
        {
            if (validationComboList())
            {
                Products myProduct = new Products();
                myProduct = (Products)cmbProduct.SelectedItem;

                Suppliers mySupplier = new Suppliers();
                mySupplier = (Suppliers)cmbSupplier.SelectedItem;

                DataAccess DB = new DataAccess();
                //calling the InsertPtoS method- inputs for this method is two objects:a product object and a supplier object
                DB.InsertPtoS(myProduct, mySupplier);
                MessageBox.Show("connection done");
                //call teh refreshlist method-this function will be refresh the list of the product-supplier in the list box
                Refreshlist();
            }
        }

        private bool validationComboList()
        {
            try
            {
                if (cmbSupplier.SelectedIndex == -1)
                {
                    MessageBox.Show("please select a supplier");
                    return false;
                }
                if (cmbProduct.SelectedIndex == -1)
                {
                    MessageBox.Show("please select product");
                    return false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return true;
        }

        //deleting the product-suppleir from the Products_Suppliers table
        private void btnDelSupProd_Click(object sender, EventArgs e)
        {
            //you have to select a Products_Suppliers from the list
            if (lstProductSupplier.SelectedIndex == -1)
            {
                MessageBox.Show("please select an item");
                return;
            }
            //be sure about deleting
            if (MessageBox.Show("Do you really want to delete?", "Delete?", MessageBoxButtons.YesNo,
                MessageBoxIcon.Question) == DialogResult.Yes)
            {
                //make myPSC object form Product_Supplier_Complete class
                Product_Supplier_Complete myPSC = new Product_Supplier_Complete();
                myPSC = (Product_Supplier_Complete)lstProductSupplier.SelectedItem;
                //make myps object from productSupplier class
                productSupplier myPS = new productSupplier();
                //add the myPSC.ps object in to the myPS objec-we have to delete product supplier from products_suppleir table
                //but the selected item in the lstproductsupplier is a object of  Product_Supplier_Complete class.
                myPS = myPSC.ps;

                try
                {
                    DataAccess Db = new DataAccess();
                    //call the DeletePS method 
                    Db.DeletePS(myPS);
                    Refreshlist();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

    }
}
