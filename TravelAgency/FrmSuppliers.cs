﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


namespace Proj207
{
    public partial class frmSuppliers : Form
    {

        public frmSuppliers(frmContainer frmTravelMain)
        {
            InitializeComponent();
            MdiParent = frmTravelMain;
        }

        private void ClearForm()
        {
            txtSupId.Text = " ";
            txtSupName.Text = " ";
            

        }

        private void refreshList()
        {
            //clear the list
            ClearForm();
            DataAccess db = new DataAccess();
            lstSuppliers.DataSource = db.SupplierSelect();
            

        }

        private void lstSuppliers_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (lstSuppliers.SelectedIndex == -1)
            {
                return;
            }
            Suppliers mysupplier = new Suppliers();


            //cast the selectedItem (object) into a Suppliers object
            mysupplier = (Suppliers)lstSuppliers.SelectedItem;
            txtSupId.Text = mysupplier.supplierID.ToString();
            txtSupName.Text = mysupplier.supplierName;
           

        }

        private void btnSupUpdate_Click(object sender, EventArgs e)
        {
            if (lstSuppliers.SelectedIndex == -1)
            {
                MessageBox.Show("please select a product to update");
                return;
            }
            if (!validateData())
            {
                return;
            }
            Suppliers mySupplier = new Suppliers();
            mySupplier.supplierID = Convert.ToInt32(txtSupId.Text);
            mySupplier.supplierName = txtSupName.Text;

            DataAccess db = new DataAccess();
            db.SupplierUpdate(mySupplier);

            List<Suppliers> mySupplierlist = db.SupplierSelect();
            try
            {
                lstSuppliers.DataSource = mySupplierlist;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }



            ClearForm();
        }

        private void btnSupAdd_Click(object sender, EventArgs e)
        {
            if (validateData())
            {
                Suppliers mySupplier = new Suppliers();

                mySupplier.supplierID = Convert.ToInt32(txtSupId.Text);
                mySupplier.supplierName  = txtSupName.Text;

                DataAccess db = new DataAccess();
                db.SupplierInsert(mySupplier);


               

                ClearForm();
                refreshList();

            }
        }

        private void btnSupDelete_Click(object sender, EventArgs e)
        {
            if (lstSuppliers.SelectedIndex == -1)
            {
                MessageBox.Show("please select a product to delete");
                return;
            }
            // confirm that they really want to delete the product
            DialogResult result = MessageBox.Show("are you sure?",
                "supplier deletion", MessageBoxButtons.YesNo,
                MessageBoxIcon.Question);

            if (result == DialogResult.No)
            {
                return; // they weren't sure about deleting the product
            }
            Suppliers mysupplier = new Suppliers();
            mysupplier.supplierID = Convert.ToInt32(txtSupId.Text);
            mysupplier.supplierName = txtSupName.Text;
            DataAccess db = new DataAccess();
            db.SupplierDelete(mysupplier);

            //string sql = "delete from Products where ProductId=@productId";

            //executeQuery(sql);
            //refresh my list
            refreshList();
            //clear the form
            ClearForm();
        }


    
        private bool validateData()
        {
            if (txtSupName.Text == " ")
            {
                MessageBox.Show("Please enter a product name");
                return false;
            }
            return true;
        }

     
        private void btnSupNew_Click(object sender, EventArgs e)
        {
            ClearForm();
        }

        private void frmSuppliers_Load(object sender, EventArgs e)
        {
            ClearForm();
            refreshList();
        }

   }
}
