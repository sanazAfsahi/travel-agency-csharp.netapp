﻿namespace Proj207
{
    partial class frmProducts
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lstProducts = new System.Windows.Forms.ListBox();
            this.grpboxProducts = new System.Windows.Forms.GroupBox();
            this.txtProdName = new System.Windows.Forms.TextBox();
            this.lblProdDesc = new System.Windows.Forms.Label();
            this.txtProdId = new System.Windows.Forms.TextBox();
            this.lblProdId = new System.Windows.Forms.Label();
            this.grpboxProdTools = new System.Windows.Forms.GroupBox();
            this.btnProdDelete = new System.Windows.Forms.Button();
            this.btnProdUpdate = new System.Windows.Forms.Button();
            this.btnProdAdd = new System.Windows.Forms.Button();
            this.btnProdNew = new System.Windows.Forms.Button();
            this.grpboxProducts.SuspendLayout();
            this.grpboxProdTools.SuspendLayout();
            this.SuspendLayout();
            // 
            // lstProducts
            // 
            this.lstProducts.FormattingEnabled = true;
            this.lstProducts.Location = new System.Drawing.Point(286, 18);
            this.lstProducts.Name = "lstProducts";
            this.lstProducts.Size = new System.Drawing.Size(294, 186);
            this.lstProducts.TabIndex = 0;
            this.lstProducts.Click += new System.EventHandler(this.lstProducts_SelectedIndexChanged);
            // 
            // grpboxProducts
            // 
            this.grpboxProducts.Controls.Add(this.txtProdName);
            this.grpboxProducts.Controls.Add(this.lblProdDesc);
            this.grpboxProducts.Controls.Add(this.txtProdId);
            this.grpboxProducts.Controls.Add(this.lblProdId);
            this.grpboxProducts.Location = new System.Drawing.Point(12, 12);
            this.grpboxProducts.Name = "grpboxProducts";
            this.grpboxProducts.Size = new System.Drawing.Size(268, 98);
            this.grpboxProducts.TabIndex = 1;
            this.grpboxProducts.TabStop = false;
            this.grpboxProducts.Text = "Product Information";
            // 
            // txtProdName
            // 
            this.txtProdName.Location = new System.Drawing.Point(112, 43);
            this.txtProdName.Name = "txtProdName";
            this.txtProdName.Size = new System.Drawing.Size(150, 20);
            this.txtProdName.TabIndex = 3;
            // 
            // lblProdDesc
            // 
            this.lblProdDesc.AutoSize = true;
            this.lblProdDesc.Location = new System.Drawing.Point(31, 46);
            this.lblProdDesc.Name = "lblProdDesc";
            this.lblProdDesc.Size = new System.Drawing.Size(75, 13);
            this.lblProdDesc.TabIndex = 2;
            this.lblProdDesc.Text = "Product Name";
            // 
            // txtProdId
            // 
            this.txtProdId.Location = new System.Drawing.Point(112, 19);
            this.txtProdId.Name = "txtProdId";
            this.txtProdId.Size = new System.Drawing.Size(150, 20);
            this.txtProdId.TabIndex = 1;
            // 
            // lblProdId
            // 
            this.lblProdId.AutoSize = true;
            this.lblProdId.Location = new System.Drawing.Point(48, 22);
            this.lblProdId.Name = "lblProdId";
            this.lblProdId.Size = new System.Drawing.Size(58, 13);
            this.lblProdId.TabIndex = 0;
            this.lblProdId.Text = "Product ID";
            // 
            // grpboxProdTools
            // 
            this.grpboxProdTools.Controls.Add(this.btnProdDelete);
            this.grpboxProdTools.Controls.Add(this.btnProdUpdate);
            this.grpboxProdTools.Controls.Add(this.btnProdAdd);
            this.grpboxProdTools.Controls.Add(this.btnProdNew);
            this.grpboxProdTools.Location = new System.Drawing.Point(12, 116);
            this.grpboxProdTools.Name = "grpboxProdTools";
            this.grpboxProdTools.Size = new System.Drawing.Size(262, 88);
            this.grpboxProdTools.TabIndex = 6;
            this.grpboxProdTools.TabStop = false;
            this.grpboxProdTools.Text = "Product Tools";
            // 
            // btnProdDelete
            // 
            this.btnProdDelete.Location = new System.Drawing.Point(153, 48);
            this.btnProdDelete.Name = "btnProdDelete";
            this.btnProdDelete.Size = new System.Drawing.Size(75, 23);
            this.btnProdDelete.TabIndex = 3;
            this.btnProdDelete.Text = "Delete";
            this.btnProdDelete.UseVisualStyleBackColor = true;
            this.btnProdDelete.Click += new System.EventHandler(this.btnProdDelete_Click);
            // 
            // btnProdUpdate
            // 
            this.btnProdUpdate.Location = new System.Drawing.Point(153, 19);
            this.btnProdUpdate.Name = "btnProdUpdate";
            this.btnProdUpdate.Size = new System.Drawing.Size(75, 23);
            this.btnProdUpdate.TabIndex = 2;
            this.btnProdUpdate.Text = "Update";
            this.btnProdUpdate.UseVisualStyleBackColor = true;
            this.btnProdUpdate.Click += new System.EventHandler(this.btnProdUpdate_Click);
            // 
            // btnProdAdd
            // 
            this.btnProdAdd.Location = new System.Drawing.Point(34, 48);
            this.btnProdAdd.Name = "btnProdAdd";
            this.btnProdAdd.Size = new System.Drawing.Size(75, 23);
            this.btnProdAdd.TabIndex = 1;
            this.btnProdAdd.Text = "Add";
            this.btnProdAdd.UseVisualStyleBackColor = true;
            this.btnProdAdd.Click += new System.EventHandler(this.btnProdAdd_Click);
            // 
            // btnProdNew
            // 
            this.btnProdNew.Location = new System.Drawing.Point(34, 19);
            this.btnProdNew.Name = "btnProdNew";
            this.btnProdNew.Size = new System.Drawing.Size(75, 23);
            this.btnProdNew.TabIndex = 0;
            this.btnProdNew.Text = "New";
            this.btnProdNew.UseVisualStyleBackColor = true;
            this.btnProdNew.Click += new System.EventHandler(this.btnProdNew_Click);
            // 
            // frmProducts
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(592, 212);
            this.Controls.Add(this.grpboxProducts);
            this.Controls.Add(this.lstProducts);
            this.Controls.Add(this.grpboxProdTools);
            this.Name = "frmProducts";
            this.Text = "Product Management";
            this.Load += new System.EventHandler(this.frmProducts_Load_1);
            this.grpboxProducts.ResumeLayout(false);
            this.grpboxProducts.PerformLayout();
            this.grpboxProdTools.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox lstProducts;
        private System.Windows.Forms.GroupBox grpboxProducts;
        private System.Windows.Forms.TextBox txtProdName;
        private System.Windows.Forms.Label lblProdDesc;
        private System.Windows.Forms.TextBox txtProdId;
        private System.Windows.Forms.Label lblProdId;
        private System.Windows.Forms.GroupBox grpboxProdTools;
        private System.Windows.Forms.Button btnProdDelete;
        private System.Windows.Forms.Button btnProdUpdate;
        private System.Windows.Forms.Button btnProdAdd;
        private System.Windows.Forms.Button btnProdNew;
    }
}