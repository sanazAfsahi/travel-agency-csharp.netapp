﻿//Program Details:
//PROJ 207 - Travel Experts C# Application
//This C# application will be used by Travel Experts employees to add, update, and delete data from their database. 
//If there more than one coder contributed, code authors will indicate their work within comments in the code. 

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Proj207
{
    public partial class frmContainer : Form
    {
        public frmContainer()
        {
            InitializeComponent();
            //frmPackages child = new frmPackages(this);
            //child.Show();
        }

        private void packagesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bool IsOpen = false;
            foreach (Form frm in Application.OpenForms)
            {
                if (frm.Text == "Packages")
                {
                    IsOpen = true;
                    frm.Focus();
                    break;
                }
            }
            if (IsOpen == false)
            {
                frmPackages PackagesChild = new frmPackages(this);
                PackagesChild.Show();
            }
        }

        private void productManagementToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bool IsOpen = false;
            foreach (Form frm in Application.OpenForms)
            {
                if (frm.Text == "Product Management")
                {
                    IsOpen = true;
                    frm.Focus();
                    break;
                }
            }
            //if (IsOpen == false)
            //{
            //    frmProducts ProductsChild = new frmProducts(this);
            //    ProductsChild.Show();
            //}
        }

        private void supplierManagementToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bool IsOpen = false;
            foreach (Form frm in Application.OpenForms)
            {
                if (frm.Text == "Supplier Management")
                {
                    IsOpen = true;
                    frm.Focus();
                    break;
                }
            }
            //if (IsOpen == false)
            //{
            //    frmSuppliers SuppliersChild = new frmSuppliers(this);
            //    SuppliersChild.Show();
            //}
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void frmContainer_Load(object sender, EventArgs e)
        {
            
        }

        private void frmContainer_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void productSupplierManagementToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bool IsOpen = false;
            foreach (Form frm in Application.OpenForms)
            {
                if (frm.Text == "Product-Supplier Management")
                {
                    IsOpen = true;
                    frm.Focus();
                    break;
                }
            }
            if (IsOpen == false)
            {
                frmProductToSupplier ProdSupplierChild = new frmProductToSupplier(this);
                ProdSupplierChild.Show();
            }
        }
    }
}
