﻿//Program Details:
//PROJ 207 - Travel Experts C# Application
//Coded by: Sanaz Afsahi
//If there more than one coder contributed, code authors will indicate their work within comments in the code. 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Proj207
{
    public class Pack_Prod_Supp
    {
        ///encapsulate properties
        private int _packageId;
        private int _productSupplierID;

        //get-set
        public int packageId
        {
            get { return _packageId; }
            set { _packageId = value; }
        }

        public int productSupplierID
        {
            get { return _productSupplierID; }
            set { _productSupplierID = value; }
        }


        public override string ToString()
        {
            return string.Format("-{0}-{1}-", packageId, productSupplierID);
        }
    }
}
