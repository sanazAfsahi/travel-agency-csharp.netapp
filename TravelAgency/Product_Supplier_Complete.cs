﻿//Program Details:
//PROJ 207 - Travel Experts C# Application
//Coded by: Sanaz Afsahi
//If there more than one coder contributed, code authors will indicate their work within comments in the code. 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Proj207
{
    public class Product_Supplier_Complete
    {
        //encapsulate properties
        private productSupplier _ps = new productSupplier();
        private Suppliers _sp = new Suppliers();
        private Products _pr = new Products();

        //get-set
        public Suppliers sp
        {
            get { return _sp; }
            set { _sp = value; }
        }
        public Products pr
        {
            get { return _pr; }
            set { _pr = value; }
        }
        public productSupplier ps
        {
            get { return _ps; }
            set { _ps = value; }
        }

        public override string ToString()
        {
            return string.Format("-{0}-{1}-", _sp.suplierName, _pr.productName);
        }


    }
}
