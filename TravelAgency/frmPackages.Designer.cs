﻿namespace Proj207
{
    partial class frmPackages
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lstPackages = new System.Windows.Forms.ListBox();
            this.dtpStartDate = new System.Windows.Forms.DateTimePicker();
            this.dtpEndDate = new System.Windows.Forms.DateTimePicker();
            this.lblName = new System.Windows.Forms.Label();
            this.txtPackName = new System.Windows.Forms.TextBox();
            this.dtpEnd = new System.Windows.Forms.Label();
            this.lblStartDate = new System.Windows.Forms.Label();
            this.lblDesc = new System.Windows.Forms.Label();
            this.txtDesc = new System.Windows.Forms.TextBox();
            this.lblPrice = new System.Windows.Forms.Label();
            this.lblCommission = new System.Windows.Forms.Label();
            this.txtCommission = new System.Windows.Forms.TextBox();
            this.txtPrice = new System.Windows.Forms.TextBox();
            this.grpboxPackage = new System.Windows.Forms.GroupBox();
            this.btnDelete = new System.Windows.Forms.Button();
            this.txtpackageId = new System.Windows.Forms.TextBox();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.lblPackageId = new System.Windows.Forms.Label();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnNew = new System.Windows.Forms.Button();
            this.grpboxProduct = new System.Windows.Forms.GroupBox();
            this.btnRemoveProduct = new System.Windows.Forms.Button();
            this.lstProductSupplier = new System.Windows.Forms.ListBox();
            this.btnAddProductSupplier = new System.Windows.Forms.Button();
            this.cboProductSuppliers = new System.Windows.Forms.ComboBox();
            this.lblIncludedProducts = new System.Windows.Forms.Label();
            this.lblAddProd = new System.Windows.Forms.Label();
            this.lblPackageList = new System.Windows.Forms.Label();
            this.grpboxPackage.SuspendLayout();
            this.grpboxProduct.SuspendLayout();
            this.SuspendLayout();
            // 
            // lstPackages
            // 
            this.lstPackages.FormattingEnabled = true;
            this.lstPackages.Location = new System.Drawing.Point(341, 28);
            this.lstPackages.Name = "lstPackages";
            this.lstPackages.Size = new System.Drawing.Size(403, 186);
            this.lstPackages.TabIndex = 0;
            this.lstPackages.SelectedIndexChanged += new System.EventHandler(this.lstPackages_SelectedIndexChanged);
            // 
            // dtpStartDate
            // 
            this.dtpStartDate.Location = new System.Drawing.Point(104, 73);
            this.dtpStartDate.Name = "dtpStartDate";
            this.dtpStartDate.Size = new System.Drawing.Size(200, 20);
            this.dtpStartDate.TabIndex = 1;
            // 
            // dtpEndDate
            // 
            this.dtpEndDate.Location = new System.Drawing.Point(104, 98);
            this.dtpEndDate.Name = "dtpEndDate";
            this.dtpEndDate.Size = new System.Drawing.Size(200, 20);
            this.dtpEndDate.TabIndex = 2;
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(7, 50);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(81, 13);
            this.lblName.TabIndex = 3;
            this.lblName.Text = "Package Name";
            // 
            // txtPackName
            // 
            this.txtPackName.Location = new System.Drawing.Point(104, 47);
            this.txtPackName.Name = "txtPackName";
            this.txtPackName.Size = new System.Drawing.Size(200, 20);
            this.txtPackName.TabIndex = 4;
            // 
            // dtpEnd
            // 
            this.dtpEnd.AutoSize = true;
            this.dtpEnd.Location = new System.Drawing.Point(36, 105);
            this.dtpEnd.Name = "dtpEnd";
            this.dtpEnd.Size = new System.Drawing.Size(52, 13);
            this.dtpEnd.TabIndex = 5;
            this.dtpEnd.Text = "End Date";
            // 
            // lblStartDate
            // 
            this.lblStartDate.AutoSize = true;
            this.lblStartDate.Location = new System.Drawing.Point(33, 79);
            this.lblStartDate.Name = "lblStartDate";
            this.lblStartDate.Size = new System.Drawing.Size(55, 13);
            this.lblStartDate.TabIndex = 6;
            this.lblStartDate.Text = "Start Date";
            // 
            // lblDesc
            // 
            this.lblDesc.AutoSize = true;
            this.lblDesc.Location = new System.Drawing.Point(28, 128);
            this.lblDesc.Name = "lblDesc";
            this.lblDesc.Size = new System.Drawing.Size(60, 13);
            this.lblDesc.TabIndex = 7;
            this.lblDesc.Text = "Description";
            // 
            // txtDesc
            // 
            this.txtDesc.Location = new System.Drawing.Point(104, 125);
            this.txtDesc.Multiline = true;
            this.txtDesc.Name = "txtDesc";
            this.txtDesc.Size = new System.Drawing.Size(200, 67);
            this.txtDesc.TabIndex = 8;
            // 
            // lblPrice
            // 
            this.lblPrice.AutoSize = true;
            this.lblPrice.Location = new System.Drawing.Point(30, 201);
            this.lblPrice.Name = "lblPrice";
            this.lblPrice.Size = new System.Drawing.Size(58, 13);
            this.lblPrice.TabIndex = 9;
            this.lblPrice.Text = "Base Price";
            // 
            // lblCommission
            // 
            this.lblCommission.AutoSize = true;
            this.lblCommission.Location = new System.Drawing.Point(26, 227);
            this.lblCommission.Name = "lblCommission";
            this.lblCommission.Size = new System.Drawing.Size(62, 13);
            this.lblCommission.TabIndex = 10;
            this.lblCommission.Text = "Commission";
            // 
            // txtCommission
            // 
            this.txtCommission.Location = new System.Drawing.Point(104, 227);
            this.txtCommission.Name = "txtCommission";
            this.txtCommission.Size = new System.Drawing.Size(200, 20);
            this.txtCommission.TabIndex = 11;
            // 
            // txtPrice
            // 
            this.txtPrice.Location = new System.Drawing.Point(104, 201);
            this.txtPrice.Name = "txtPrice";
            this.txtPrice.Size = new System.Drawing.Size(200, 20);
            this.txtPrice.TabIndex = 12;
            // 
            // grpboxPackage
            // 
            this.grpboxPackage.Controls.Add(this.btnDelete);
            this.grpboxPackage.Controls.Add(this.txtpackageId);
            this.grpboxPackage.Controls.Add(this.btnUpdate);
            this.grpboxPackage.Controls.Add(this.lblPackageId);
            this.grpboxPackage.Controls.Add(this.btnAdd);
            this.grpboxPackage.Controls.Add(this.lblDesc);
            this.grpboxPackage.Controls.Add(this.txtPrice);
            this.grpboxPackage.Controls.Add(this.btnNew);
            this.grpboxPackage.Controls.Add(this.txtCommission);
            this.grpboxPackage.Controls.Add(this.dtpStartDate);
            this.grpboxPackage.Controls.Add(this.dtpEndDate);
            this.grpboxPackage.Controls.Add(this.txtDesc);
            this.grpboxPackage.Controls.Add(this.lblCommission);
            this.grpboxPackage.Controls.Add(this.txtPackName);
            this.grpboxPackage.Controls.Add(this.lblStartDate);
            this.grpboxPackage.Controls.Add(this.lblName);
            this.grpboxPackage.Controls.Add(this.lblPrice);
            this.grpboxPackage.Controls.Add(this.dtpEnd);
            this.grpboxPackage.Location = new System.Drawing.Point(9, 12);
            this.grpboxPackage.Name = "grpboxPackage";
            this.grpboxPackage.Size = new System.Drawing.Size(787, 254);
            this.grpboxPackage.TabIndex = 13;
            this.grpboxPackage.TabStop = false;
            this.grpboxPackage.Text = "Package Details";
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(660, 217);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 23);
            this.btnDelete.TabIndex = 19;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // txtpackageId
            // 
            this.txtpackageId.Location = new System.Drawing.Point(104, 19);
            this.txtpackageId.Name = "txtpackageId";
            this.txtpackageId.Size = new System.Drawing.Size(200, 20);
            this.txtpackageId.TabIndex = 9;
            // 
            // btnUpdate
            // 
            this.btnUpdate.Location = new System.Drawing.Point(560, 217);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(75, 23);
            this.btnUpdate.TabIndex = 18;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // lblPackageId
            // 
            this.lblPackageId.AutoSize = true;
            this.lblPackageId.Location = new System.Drawing.Point(24, 22);
            this.lblPackageId.Name = "lblPackageId";
            this.lblPackageId.Size = new System.Drawing.Size(64, 13);
            this.lblPackageId.TabIndex = 8;
            this.lblPackageId.Text = "Package ID";
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(453, 217);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 23);
            this.btnAdd.TabIndex = 17;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAddPackage_Click);
            // 
            // btnNew
            // 
            this.btnNew.Location = new System.Drawing.Point(332, 217);
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(75, 23);
            this.btnNew.TabIndex = 16;
            this.btnNew.Text = "New";
            this.btnNew.UseVisualStyleBackColor = true;
            this.btnNew.Click += new System.EventHandler(this.btnNewPackage_Click);
            // 
            // grpboxProduct
            // 
            this.grpboxProduct.Controls.Add(this.btnRemoveProduct);
            this.grpboxProduct.Controls.Add(this.lstProductSupplier);
            this.grpboxProduct.Controls.Add(this.btnAddProductSupplier);
            this.grpboxProduct.Controls.Add(this.cboProductSuppliers);
            this.grpboxProduct.Controls.Add(this.lblIncludedProducts);
            this.grpboxProduct.Controls.Add(this.lblAddProd);
            this.grpboxProduct.Location = new System.Drawing.Point(9, 285);
            this.grpboxProduct.Name = "grpboxProduct";
            this.grpboxProduct.Size = new System.Drawing.Size(787, 178);
            this.grpboxProduct.TabIndex = 15;
            this.grpboxProduct.TabStop = false;
            this.grpboxProduct.Text = "Products-Suppliers";
            // 
            // btnRemoveProduct
            // 
            this.btnRemoveProduct.Location = new System.Drawing.Point(250, 111);
            this.btnRemoveProduct.Name = "btnRemoveProduct";
            this.btnRemoveProduct.Size = new System.Drawing.Size(75, 23);
            this.btnRemoveProduct.TabIndex = 4;
            this.btnRemoveProduct.Text = "Remove";
            this.btnRemoveProduct.UseVisualStyleBackColor = true;
            this.btnRemoveProduct.Click += new System.EventHandler(this.btnRemoveProduct_Click);
            // 
            // lstProductSupplier
            // 
            this.lstProductSupplier.FormattingEnabled = true;
            this.lstProductSupplier.Location = new System.Drawing.Point(423, 43);
            this.lstProductSupplier.Name = "lstProductSupplier";
            this.lstProductSupplier.Size = new System.Drawing.Size(274, 121);
            this.lstProductSupplier.TabIndex = 23;
            // 
            // btnAddProductSupplier
            // 
            this.btnAddProductSupplier.Location = new System.Drawing.Point(147, 111);
            this.btnAddProductSupplier.Name = "btnAddProductSupplier";
            this.btnAddProductSupplier.Size = new System.Drawing.Size(75, 23);
            this.btnAddProductSupplier.TabIndex = 3;
            this.btnAddProductSupplier.Text = "Add";
            this.btnAddProductSupplier.UseVisualStyleBackColor = true;
            this.btnAddProductSupplier.Click += new System.EventHandler(this.btnAddProductSupplier_Click);
            // 
            // cboProductSuppliers
            // 
            this.cboProductSuppliers.FormattingEnabled = true;
            this.cboProductSuppliers.Location = new System.Drawing.Point(125, 63);
            this.cboProductSuppliers.Name = "cboProductSuppliers";
            this.cboProductSuppliers.Size = new System.Drawing.Size(229, 21);
            this.cboProductSuppliers.TabIndex = 2;
            // 
            // lblIncludedProducts
            // 
            this.lblIncludedProducts.AutoSize = true;
            this.lblIncludedProducts.Location = new System.Drawing.Point(420, 16);
            this.lblIncludedProducts.Name = "lblIncludedProducts";
            this.lblIncludedProducts.Size = new System.Drawing.Size(96, 13);
            this.lblIncludedProducts.TabIndex = 5;
            this.lblIncludedProducts.Text = "Included Products:";
            // 
            // lblAddProd
            // 
            this.lblAddProd.AutoSize = true;
            this.lblAddProd.Location = new System.Drawing.Point(122, 33);
            this.lblAddProd.Name = "lblAddProd";
            this.lblAddProd.Size = new System.Drawing.Size(85, 13);
            this.lblAddProd.TabIndex = 0;
            this.lblAddProd.Text = "Product-Supplier";
            // 
            // lblPackageList
            // 
            this.lblPackageList.AutoSize = true;
            this.lblPackageList.Location = new System.Drawing.Point(387, 9);
            this.lblPackageList.Name = "lblPackageList";
            this.lblPackageList.Size = new System.Drawing.Size(86, 13);
            this.lblPackageList.TabIndex = 21;
            this.lblPackageList.Text = "Package Listing:";
            // 
            // frmPackages
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(976, 483);
            this.Controls.Add(this.lblPackageList);
            this.Controls.Add(this.grpboxProduct);
            this.Controls.Add(this.lstPackages);
            this.Controls.Add(this.grpboxPackage);
            this.Name = "frmPackages";
            this.Text = "Packages";
            this.Load += new System.EventHandler(this.frmPackages_Load);
            this.grpboxPackage.ResumeLayout(false);
            this.grpboxPackage.PerformLayout();
            this.grpboxProduct.ResumeLayout(false);
            this.grpboxProduct.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox lstPackages;
        private System.Windows.Forms.DateTimePicker dtpStartDate;
        private System.Windows.Forms.DateTimePicker dtpEndDate;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.TextBox txtPackName;
        private System.Windows.Forms.Label dtpEnd;
        private System.Windows.Forms.Label lblStartDate;
        private System.Windows.Forms.Label lblDesc;
        private System.Windows.Forms.TextBox txtDesc;
        private System.Windows.Forms.Label lblPrice;
        private System.Windows.Forms.Label lblCommission;
        private System.Windows.Forms.TextBox txtCommission;
        private System.Windows.Forms.TextBox txtPrice;
        private System.Windows.Forms.GroupBox grpboxPackage;
        private System.Windows.Forms.GroupBox grpboxProduct;
        private System.Windows.Forms.Button btnRemoveProduct;
        private System.Windows.Forms.Button btnAddProductSupplier;
        private System.Windows.Forms.ComboBox cboProductSuppliers;
        private System.Windows.Forms.Label lblAddProd;
        private System.Windows.Forms.Label lblIncludedProducts;
        private System.Windows.Forms.Button btnNew;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Label lblPackageList;
        private System.Windows.Forms.TextBox txtpackageId;
        private System.Windows.Forms.Label lblPackageId;
        private System.Windows.Forms.ListBox lstProductSupplier;
    }
}