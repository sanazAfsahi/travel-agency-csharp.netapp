﻿//Program Details:
//PROJ 207 - Travel Experts C# Application
//Coded by: Sanaz Afsahi
//If there more than one coder contributed, code authors will indicate their work within comments in the code. 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Proj207
{
    public class Packages
    {
        #region Private Variables

        private int _packageId;
        private string _packageName;
        private DateTime _pkgStartDate;
        private DateTime _pkgEndDate;
        private string _pkgDescrip;
        private decimal _pkgBasePrice;
        private decimal _pkgAgencyCommission;

        #endregion

        #region Properties

        public int packageId
        {
            get { return _packageId; }
            set { _packageId = value; }
        }

        public string packageName
        {
            get { return _packageName; }
            set { _packageName = value; }
        }

        public DateTime pkgStartDate
        {
            get { return _pkgStartDate; }
            set { _pkgStartDate = value; }
        }
        public DateTime pkgEndDate
        {
            get { return _pkgEndDate; }
            set { _pkgEndDate = value; }
        }

        public string pkgDescrip
        {
            get { return _pkgDescrip; }
            set { _pkgDescrip = value; }
        }
        public decimal pkgBasePrice
        {
            get { return _pkgBasePrice; }
            set { _pkgBasePrice = value; }
        }
        public decimal pkgAgencyCommission
        {
            get { return _pkgAgencyCommission; }
            set { _pkgAgencyCommission = value; }
        }

        #endregion

        public override string ToString()
        {
            return string.Format("-{0}-{1}-{2}-{3}--{4}--{5}-{6}-", packageId, packageName, pkgStartDate, pkgEndDate, pkgDescrip, pkgBasePrice, pkgAgencyCommission);
        }

    }
}
