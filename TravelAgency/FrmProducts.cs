﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


namespace Proj207
{
    public partial class frmProducts : Form
    {
        
        public frmProducts(frmContainer frmTravelMain)
        {
            InitializeComponent();
            MdiParent = frmTravelMain;
        }
      
        private void ClearForm()
        {
            txtProdId.Text = " ";
            txtProdName.Text = " ";

        }

        private void refreshList()
        {
            //clear the list
          
            DataAccess db = new DataAccess();
            lstProducts.DataSource = db.ProductSelect();

        }

        private void lstProducts_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (lstProducts.SelectedIndex == -1)
            {
                return;
            }
            Products myProduct = new Products();

            //cast the selectedItem (object) into a Products object
            myProduct = (Products)lstProducts.SelectedItem;
            txtProdId.Text = myProduct.productId.ToString();
            txtProdName.Text = myProduct.productName;
            txtProdId.ReadOnly = true;
            

        }

        

        private void btnProdAdd_Click(object sender, EventArgs e)
        {
            if (validateData())
            {
                Products myProduct = new Products();
               // myProduct.productId = Convert.ToInt32(txtProdId.Text);
                myProduct.productName = txtProdName.Text; 

                DataAccess db = new DataAccess();
                db.ProductInsert(myProduct);

                
                txtProdId.Enabled = false;
               
                ClearForm();
                refreshList();

            }

        }

        private bool validateData()
        {
            if (txtProdName.Text == " ")
            {
                MessageBox.Show("Please enter a product name");
                return false;
            }
            return true;
        }

        private void btnProdUpdate_Click(object sender, EventArgs e)
        {

            if (lstProducts.SelectedIndex == -1)
            {
                MessageBox.Show("please select a product to update");
                return;
            }
            if (!validateData())
            {
                return;
            }
            Products myProduct = new Products();
            myProduct.productId = Convert.ToInt32(txtProdId.Text);
            myProduct.productName = txtProdName.Text;
            DataAccess db = new DataAccess();
            db.ProductUpdate(myProduct);
            List<Products> myProductlist = db.ProductSelect();
            try
                {
                   lstProducts.DataSource = myProductlist;
                }
            catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

                    
               
             ClearForm();

            }
            

        private void btnProdDelete_Click(object sender, EventArgs e)
        {
            if (lstProducts.SelectedIndex == -1)
            {
                MessageBox.Show("please select a product to delete");
                return;
            }
            // confirm that they really want to delete the product
            DialogResult result = MessageBox.Show("are you sure?",
                "product deletion", MessageBoxButtons.YesNo,
                MessageBoxIcon.Question);

            if (result == DialogResult.No)
            {
                return; // they weren't sure about deleting the product
            }
            Products myProduct = new Products();
            myProduct.productId = Convert.ToInt32(txtProdId.Text);
            myProduct.productName = txtProdName.Text;
            DataAccess db = new DataAccess();
            db.ProductDelete(myProduct);

            //string sql = "delete from Products where ProductId=@productId";

            //executeQuery(sql);
            //refresh my list
            refreshList();
            //clear the form
            ClearForm();
        }

        //private void frmProducts_Load(object sender, EventArgs e)
        //{

        //    refreshList();
        //}

        private void btnProdNew_Click(object sender, EventArgs e)
        {
            ClearForm();
        }

        private void frmProducts_Load_1(object sender, EventArgs e)
        {
            txtProdId.ReadOnly = true;
            refreshList();
        }

        

    }
}
