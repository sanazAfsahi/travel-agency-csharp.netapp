﻿//Program Details:
//PROJ 207 - Travel Experts C# Application
//Coded by: Sanaz Afsahi
//If there more than one coder contributed, code authors will indicate their work within comments in the code. 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Proj207
{
    public class Products
    {

        private int _productId;
        private string _productName;

        public int productId
        {
            get { return _productId; }
            set { _productId = value; }
        }

        public string productName
        {
            get { return _productName; }
            set { _productName = value; }
        }


        public override string ToString()
        {
            return string.Format("-{0}-{1}-", productId, productName);
        }
    }
}
